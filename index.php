<?php

$data = file_get_contents('https://enternet.lt/products.json');

$array = json_decode($data);

$xml            = new SimpleXMLElement('<xml/>');
$weight_xml     = $xml->addChild('weight');
$price_xml      = $xml->addChild('price');
$zero_stock_xml = $xml->addChild('zero_stock');

$max_weight   = 0;
$min_weight   = 0;
$total_weight = 0;
$prices       = [];

foreach ($array as $row) {
    if ($row['active']) {
        $total_weight += $row['weight'] * $row['stock'];

        if ($row['weight'] > $max_weight) {
            $max_weight = $row['weight'];
        } elseif ($row['weight'] < $min_weight) {
            $min_weight = $row['weight'];
        }

        $prices[] = $row['price'];

        if ($row['stock'] <= 0) {
            $zero_stock_xml->addAttribute('ean', $row['ean13']);
        }
    }
}

$weight_xml->addAttribute('min', $min_weight);
$weight_xml->addAttribute('max', $max_weight);
$weight_xml->addAttribute('sum', $total_weight);
$weight_xml->addAttribute('avg', $total_weight / count($array));

$price_xml->addAttribute('avg', array_sum($prices) / count($prices));

echo $xml->asXML();
